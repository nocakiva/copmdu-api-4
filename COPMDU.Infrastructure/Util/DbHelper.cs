﻿using COPMDU.Domain;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace COPMDU.Infrastructure.Util
{
    /// <summary>
    /// Funções de ajudar a comunicação com a base de dados.
    /// </summary>
    public class DbHelper
    {
        private readonly CopMduDbContext _db;
        private readonly IConfiguration _config;

        private string DefaultPasswordRecoverEmail => _config["CopMdu:DefaultPasswordRecoverEmail"];

        public DbHelper(CopMduDbContext db, IConfiguration config)
        {
            _db = db;
            _config = config;
        }


        /// <summary>
        /// Retorna o usuário encontrado caso não exista na base de dados adiciona ele a base de dados.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="users"></param>
        /// <param name="dbUsers"></param>
        /// <returns></returns>
        public User ReturnUser(string userName, List<User> users, DbSet<User> dbUsers)
        {
            if (string.IsNullOrEmpty(userName))
                return null;

            //format username 
            userName = userName.ToUpper().Trim();

            if (!users.Exists(u => u.Username == userName))
            {
                var newUser = new User
                {
                    Username = userName,
                    Name = userName,
                    Password = "SC" + userName,
                    Email = DefaultPasswordRecoverEmail,
                    ChangePassword = true,
                };
                dbUsers.Add(newUser);
                _db.SaveChanges();
                users.Add(newUser);

            }
            return users.Find(u => u.Username == userName);
        }

        private T DefaultBaseValue<T>(string name) where T : Base
        {
            var returnObj = (T)Activator.CreateInstance(typeof(T));
            returnObj.Name = name;
            return returnObj;
        }

        /// <summary>
        /// Retorna o dado procurando pelo nome
        /// </summary>
        public T GetByName<T>(List<T> data, string name) where T : Base =>
            name == null ?
            null :
            data.FirstOrDefault(d => d.Name == name);

        /// <summary>
        /// Procura dado pelo nome caso não exista adiciona a base de dados.
        /// </summary>
        public T ReturnBase<T>(string name, List<T> data, DbSet<T> st) where T : Base
        {
            if (!data.Exists(d => d.Name == name))
            {
                var newData = DefaultBaseValue<T>(name);
                st.Add(newData);
                _db.SaveChanges();
                data.Add(newData);
            }
            return GetByName(data, name);
        }

    }
}
