﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using System.Threading;
using Microsoft.Extensions.Configuration;
using COPMDU.Infrastructure.Interface;
using COPMDU.Infrastructure.Repository;
using COPMDU.Infrastructure.Util;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;

namespace COPMDU.Infrastructure.Services
{
    public class OutageUpdateService : BackgroundService
    {
        private readonly IConfiguration _config;
        private readonly ILogger _logger;
        private readonly IServiceScopeFactory _scope;

        private int _updateTime => int.Parse(_config["ServicesUpdate:Outage:Time"]);
        private bool _enabled => bool.Parse(_config["ServicesUpdate:Outage:Enabled"]);
        private string _username => _config["Credentials:NewMonitor:User"];
        private string _password => _config["Credentials:NewMonitor:Password"];

        public OutageUpdateService(
            ILogger<OutageUpdateService> logger, 
            IConfiguration config, 
            IServiceScopeFactory serviceScopeFactory
            )
        {
            _config = config;
            _logger = logger;
            _scope = serviceScopeFactory;
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                await MigrateDb();
            }
            catch (Exception)
            {
                //Falha ao gerar scope não irá carregar nenhum serviço para fazer o acompanhamento.
            }

            if (_enabled)
            {
                _logger.LogDebug("Iniciado serviço de Atualização de Outage.");
                while (!stoppingToken.IsCancellationRequested)
                {
                    await LoadOutagesToDb(stoppingToken);
                }
                _logger.LogDebug("Finalizado serviço de Atualização de Outage.");
            }
        }

        private async Task LoadOutagesToDb(CancellationToken stoppingToken)
        {
            using (var scope = _scope.CreateScope())
            {
                var ticketRepository = (ITicketRepository)scope.ServiceProvider.GetRequiredService(typeof(ITicketRepository));
                var db = scope.ServiceProvider.GetRequiredService<CopMduDbContext>();
                var logRepository = scope.ServiceProvider.GetRequiredService<ILogRepository>();
                _logger.LogDebug("Rodando atualização de Outage.");
                try
                {
                    await ticketRepository.Authenticate(_username, _password);
                    await ticketRepository.LoadNewOutage();
                }
                catch (Exception ex)
                {
                    db.Outage.Local.ToList().ForEach(o => db.Entry(o).State = EntityState.Detached);
                    logRepository.RecordException(ex);
                }
                await Task.Delay(_updateTime, stoppingToken);
            }
        }

        private async Task MigrateDb()
        {
            using (var scope = _scope.CreateScope())
            {
                var ticketRepository = (ITicketRepository)scope.ServiceProvider.GetRequiredService(typeof(ITicketRepository));
                var db = scope.ServiceProvider.GetRequiredService<CopMduDbContext>();
                await db.Database.MigrateAsync();
            }
        }
    }
}
