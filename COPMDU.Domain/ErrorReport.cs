﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain
{
    public class ErrorReport
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public int Ticket { get; set; }

        public int? UserId { get; set; }

        public User User { get; set; }

        public int? ErrorReportReasonId { get; set; }

        public ErrorReportReason ErrorReportReason { get; set; }

        public DateTime Date { get; set; }
    }
}